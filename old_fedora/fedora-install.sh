#!/bin/bash

src="/home/faust"
src1="/home/faust/learningit/gitlab/dotfiles/.xinitrc"
src2="/home/faust/learningit/gitlab/dotfiles/.Xresources"
src3="/home/faust/learningit/gitlab/dotfiles/.zshrc"
src4="/home/faust/learningit/gitlab/dotfiles/.config"

$sudo cp -r "$src1" "$src"
$sudo cp -r "$src2" "$src"
$sudo cp -r "$src3" "$src"
$sudo cp -r "$src4" "$src"

echo "###############################"
echo "##### .dotfiles Installed #####"
echo "###############################"

$sudo dnf update

echo "###########################"
echo "##### System Updated  #####"
echo "###########################"

$sudo dnf install -y @base-x bspwm sxhkd dmenu rxvt-unicode-256color neovim firefox nnn htop neofetch feh wget curl terminus-fonts

echo "##############################################"
echo "##### Window Manager Packages Installed  #####"
echo "##############################################"

$sudo dnf install -y dkms binutils gcc make cmake automake patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel bzip2 perl

echo "##############################################"
echo "##### Guest Addition Packages Installed  #####"
echo "##############################################"
