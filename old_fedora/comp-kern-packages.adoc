= Installing Packages needed for Compiling or VirtualBox Guest Additions
:toc:
:toclevels:	4

== link:https://forums.virtualbox.org/viewtopic.php?t=15679[VirtualBox Guest Additions] Packages

----
$ dnf install dkms binutils gcc make cmake automake patch libgomp glibc-headers glibc-devel kernel-headers kernel-devel bzip2 perl
----

== Install link:https://download.virtualbox.org/virtualbox/[Guest Additions]

Mount and Install

----
$ mkdir /media/virtualboxguestadditions #as root

$ mount /path/to/downloaded/file /media/virtualboxguestadditions/ #as root

$ cd /media/virtualboxguestadditions

$ ./VBoxLinuxAdditions.run #as root

$ reboot
----
