= ZSH

Install `zsh` and `zsh-syntax-highlighting`

----
$ sudo dnf install zsh zsh-syntax-highlighting
----

Install oh-my-zsh via wget

----
$ sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
----

Install `zsh-autosuggestions`

----
$ git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
----

Edit `.zshrc`

----
$ nvim ~/.zshrc

source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
----

Reload `.zshrc`

----
$ source ~/.zshrc
----

Alias to reload `.Xresources`

----
$ nvim ~/.zshrc

alias xup="xrdb ~/.Xresources"
----
